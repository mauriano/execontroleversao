#include "Media.h"
#include <iostream>

using namespace std;

float Media::getNotaGa(){
    return notaGa;
}
float Media::getNotaGb(){
    return notaGb;
}

void Media::setNotaGa(float a){
    notaGa = a;
    if(notaGa < 0)
        notaGa = 0;
    if(notaGa > 10)
        notaGa = 10;
}
void Media::setNotaGb(float b){
    notaGb = b;
    if(notaGb < 0)
        notaGb = 0;
    if(notaGb > 10)
        notaGb = 10;
}

void Media::calculaMedia(){
    float notaFinal = (notaGa * 1 + notaGb * 2) / 3;

    if(notaFinal >= 6){
        cout << "Passou! Nota final: " << notaFinal << endl;
    }else{
        float notaNecessariaA = 18 - 2 * notaGb;
        float notaNecessariaB = (18 - 1 * notaGa) / 2;
        float notaNecessaria = (notaNecessariaB > notaNecessariaA) ? notaNecessariaA : notaNecessariaB;
        cout << "N�o Passou! Nota necessaria: " << notaNecessaria << endl;

    }
}

Media::Media(float a, float b){
    setNotaGa(a);
    setNotaGb(b);
}

Media::Media()
{
    setNotaGa(0);
    setNotaGb(0);
}

Media::~Media()
{
    //dtor
}
