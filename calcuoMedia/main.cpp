#include <iostream>

#include<Media.h>

using namespace std;

int main()
{
    float notaGa;
    float notaGb;

    cout << "Digite a nota do GA: " << endl;
    cin >> notaGa;
    cout << "Digite a nota do GB: " << endl;
    cin >> notaGb;

    Media media = Media(notaGa, notaGb);
    media.calculaMedia();

    return 0;
}
