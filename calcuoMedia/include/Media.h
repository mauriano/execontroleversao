#ifndef MEDIA_H
#define MEDIA_H


class Media
{
    public:

        float getNotaGa();
        float getNotaGb();

        void setNotaGa(float a);
        void setNotaGb(float b);

        void calculaMedia();

        Media();

        Media(float a, float b);

        virtual ~Media();

    protected:

    private:
        float notaGa;
        float notaGb;
};

#endif // MEDIA_H
